import react, { Fragment, useState, useRef, useEffect } from "react";
import ReactDOM from "react-dom";
import { PostItItem } from "./PostItItem";
import { v4 as uuid } from "uuid";

const KEY = "postitlist-postits";

export function PostItList() {
  const [postits, setPostits] = useState([
    { id: 1, post: "Tarea 1", contenido: "Contenido 1", importante: false },
    { id: 2, post: "Tarea 2", contenido: "Contenido 2", importante: true },
    { id: 3, post: "Tarea 3", contenido: "Contenido 3", importante: true },
  ]);

  const postitRef = useRef();
  const postitContent = useRef();
  const postitCheck = useRef();

  useEffect(() => {
    const storedPostits = JSON.parse(localStorage.getItem(KEY));
    if (storedPostits) {
      setPostits(storedPostits);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem(KEY, JSON.stringify(postits));
  }, [postits]);


  const agregarPostit = () => {
    console.log("Agregando Tarea");
    var post = postitRef.current.value;
    if(post.length === 0){
      post="Sin Título";
    }
    const contenido = postitContent.current.value;
    if (contenido.length === 0) {
      alert("El campo descripción es obligatorio.");
      document.getElementById("miDescripcion").focus();
      postitRef.current.value = null;
      postitContent.current.value = null;
      return false;
    }
    const id = uuid();
    let vImportante = true;
    let estadomicheck = document.getElementById("miCheck");

    if (estadomicheck.checked) {
      vImportante = true;
      console.log("ESTÁ CLICKEADO");
    } else {
      vImportante = false;
      console.log("NO ESTÁ CLICKEADO");
    }

    console.log(post);
    console.log(contenido);
    console.log(id);
    console.log(vImportante);

    if (post === "") return;

    setPostits((prevPostits) => {
      const newPostit = {
        id: uuid(),
        post: post,
        contenido: contenido,
        importante: vImportante,
      };

      return [...prevPostits, newPostit];
    });

    postitRef.current.value = null;
    postitContent.current.value = null;
  };

  const MiChecker = () => {
    return (
      <div className="form-check">
        <input
          ref={postitCheck}
          id="miCheck"
          type="checkbox"
          className="form-check-input"
        ></input>
        <label
          style={{ color: "#FFFFFF" }}
          className="form-check-label"
          htmlFor="flexCheckDefault"
        >
          Importante!
        </label>
      </div>
    );
  };

  const eliminarPostItPizarra = (id) => {
    console.log(id);
    const newPostits = [...postits];
    for (var i = 0; i < newPostits.length; i++) {
      if (newPostits[i].id === id) {
        newPostits.splice(i, 1);
      }
    }
    setPostits(newPostits);
  };

  return (
    <Fragment>
      <div>
        <h1 className="fw-bold">Post It Simulator!</h1>

        <div className="input-group mt-4 mb-4">
          <div className="col-2 ms-3">
            <input
              ref={postitRef}
              placeholder="Título"
              className="form-control"
              type="text"
            ></input>
          </div>

          <div className="col-4 ms-3">
            <input
              id="miDescripcion"
              ref={postitContent}
              placeholder="Descripción"
              className="form-control"
              type="text"
            ></input>
          </div>

          <div className="col-1 ms-3">
            <MiChecker></MiChecker>
          </div>

          <div className="col-3 ms-3 d-grid gap-2">
            <button onClick={agregarPostit} className="btn btn-dark">
              AGREGAR
            </button>
          </div>

        </div>

        <div className="col">
            <ul className="col-xs-12 col-sm-10 col-md-10 col-lg-10">
              {postits.map((postit) => (
                <PostItItem
                postit={postit}
                  key={postit.id}
                  eliminarPostIt={eliminarPostItPizarra}
                ></PostItItem>
              ))}
            </ul>
          </div>
      </div>
    </Fragment>
  );
}
