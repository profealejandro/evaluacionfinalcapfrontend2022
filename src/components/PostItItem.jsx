import react, { Fragment } from "react";
import ReactDOM from "react-dom";

export function PostItItem({ postit, eliminarPostIt }) {
  const { id, post, contenido, importante } = postit;

  const fnEliminarPostIt = () => {
    eliminarPostIt(id);
  };

  if (importante === true) {
    return (
      <div className="col curvado">
        <li className="importante">
          <div className="input-group ">
            <div className="col-11">
              <h2>{post}</h2>
            </div>

            <div className="col-1">
              <button
                className="btn-close pull-right"
                onClick={fnEliminarPostIt}
                type="button"
              ></button>
            </div>
          </div>

          <p>{contenido}</p>
        </li>
      </div>
    );
  } else {
    return (
      <div className="col curvado">
        <li className="normal">
          <div className="input-group ">
            <div className="col-11">
              <h2>{post}</h2>
            </div>

            <div className="col-1">
              <button
                className="btn-close pull-right"
                onClick={fnEliminarPostIt}
                type="button"
              ></button>
            </div>
          </div>

          <p>{contenido}</p>
        </li>
      </div>
    );
  }
}
