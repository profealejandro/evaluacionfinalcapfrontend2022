import react from 'react'
import ReactDOM from 'react-dom'
import { App } from './App';
import './index.css';

const element = <h1>Hola Mundo!</h1>;
const rootElement = document.getElementById("root");

ReactDOM.render(<App/>, rootElement);